package com.trident.insurtech.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/quotes")
public class TridentAPIController {

    @GetMapping("/quote")
    public String getQuote(){
        return "Trident API Service... ";
    }


}
